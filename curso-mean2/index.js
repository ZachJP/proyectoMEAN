'use strict'

/*Conexion a MongoDB*/
var mongoose = require('mongoose');
var app =require('./app');
var port= process.env.PORT || 3977;
mongoose.connect('mongodb://localhost:27017/curso-mean2',(err,res)=>{
  if(err){
    throw err;
  }else{
    console.log("La conexión a la BD esta corriendo correctamente ...");
    app.listen(port,function(){
        console.log("Servidor del API rest de Musica escuchando en http://localhost:"+port);
    });
  }
});
