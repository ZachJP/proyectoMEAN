'use strict'

var express=require('express');
var bodyParser=require('body-parser');

var app=express();

/*Cargar Rutas*/
var user_routes=require('./rutas/usuarioRutas');
var artist_routes=require('./rutas/artistaRutas');
var album_routes=require('./rutas/albumRutas');
var song_routes=require('./rutas/cancionRutas');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

/*Configurar cabeceras de http*/
app.use((req,res,next)=>{
  res.header('Access-Control-Allow-Origin','*');
  res.header('Access-Control-Allow-Headers',
  'Authorization, X-API-KEY,X-Requested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods','GET,POST,PUT,OPTIONS,DELETE');
  res.header('Allow','GET,POST,OPTIONS,PUT,DELETE');

  next();
});

/*Rutas Base*/
app.use('/api', user_routes);
app.use('/api', artist_routes);
app.use('/api', album_routes);
app.use('/api',song_routes);
// app.get('/pruebas',function(req,res){
//   res.status(200).send({
//     message: 'Bienvenido Zach, al servidor express! ^^'
//   });
// });

module.exports=app;
