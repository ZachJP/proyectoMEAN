'use strict'

var jwt=require('jwt-simple');
var moment=require('moment');
var secret='clave_secreta_shhh';

exports.createToken=function(usuario){
  var payload={
    sub: usuario._id,
    name: usuario.nombre,
    surname: usuario.nombreCompleto,
    email: usuario.correo,
    role:usuario.rol,
    image:usuario.imagen,
    iat:moment().unix(),
    exp:moment().add(30,'days').unix
  }; //DAtos a codificar
  return jwt.encode(payload, secret);
};
