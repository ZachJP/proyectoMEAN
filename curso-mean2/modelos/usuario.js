'use strict'

var mongoose= require('mongoose');
var Schema =mongoose.Schema;

var UsuarioSchema=Schema({
  nombre:String,
  nombreCompleto:String,
  correo:String,
  contra:String,
  rol:String,
  imagen:String
});

module.exports = mongoose.model('Usuario',UsuarioSchema)
