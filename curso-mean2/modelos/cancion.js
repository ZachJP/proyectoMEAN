'use strict'

var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var CancionSchema = Schema({
  numPista: String,
  nombreTema: String,
  duracion:String,
  pistaAudio:String,
  imagen:String,
  album: {type:Schema.ObjectId, ref: 'Album'}
});

module.exports= mongoose.model('Cancion',CancionSchema);
