'use strict'

var express=require('express');
var ArtistController=require('../controllers/artistaController');
var api=express.Router();

var md_auth=require('../middleware/midAutenticacion');

var multipart=require('connect-multiparty');//trabajar con la subida de ficheros
var md_upload=multipart({uploadDir: './uploads/artistaImagenes'});

api.get('/obtenerArtista/:id',md_auth.ensureAuth,ArtistController.obtenerArtista);
api.post('/crearArtista',md_auth.ensureAuth,ArtistController.crearArtista);
api.get('/obtenerTodosArtistas/:page?',md_auth.ensureAuth,ArtistController.obtenerArtistaTodo);
api.put('/actualizarArtista/:id',md_auth.ensureAuth,ArtistController.actualizarArtista);
api.delete('/borrarArtista/:id',md_auth.ensureAuth,ArtistController.borrarArtista);
api.post('/subirImagenArtista/:id',[md_upload,md_auth.ensureAuth],ArtistController.subirImagen);
api.get('/obtenerImagenArtista/:imageFile',md_auth.ensureAuth,ArtistController.obtenerImagen);

module.exports=api;
