'use strict'

var express=require('express');
var AlbumController=require('../controllers/albumController');
var api=express.Router();

var md_auth=require('../middleware/midAutenticacion');

var multipart=require('connect-multiparty');//trabajar con la subida de ficheros
var md_upload=multipart({uploadDir: './uploads/albumImagenes'});
api.get('/obtenerAlbum/:id',md_auth.ensureAuth,AlbumController.obtenerAlbum);
api.post('/crearAlbum',md_auth.ensureAuth,AlbumController.crearAlbum);
api.get('/mostrarAlbums/:artista?',md_auth.ensureAuth,AlbumController.mostrarAlbums);
api.put('/actualizarAlbum/:id',md_auth.ensureAuth,AlbumController.actualizarAlbum);
api.delete('/borrarAlbum/:id',md_auth.ensureAuth,AlbumController.borrarAlbum);
api.post('/subirImagenAlbum/:id',[md_upload,md_auth.ensureAuth],AlbumController.subirImagen);
api.get('/obtenerImagenAlbum/:imageFile',md_auth.ensureAuth,AlbumController.obtenerImagen);

module.exports=api;
