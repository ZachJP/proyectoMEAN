'use strict'

var express = require('express');
var UserController = require('../controllers/usuarioController');
var api = express.Router();

var md_auth=require('../middleware/midAutenticacion');

var multipart=require('connect-multiparty');//trabajar con la subida de ficheros
var md_upload=multipart({uploadDir: './uploads/usuariosImagenes'});

api.get('/probando-controlador', md_auth.ensureAuth ,UserController.pruebas);
api.post('/registrarUsuario',UserController.crearUsuario);
api.put('/actualizarUsuario/:id',md_auth.ensureAuth,UserController.actualizarUsuario);
api.post('/loginUsuario',UserController.loginUsuario);
api.post('/subirImagen/:id',[md_auth.ensureAuth,md_upload],UserController.subirImagen);
api.get('/obtenerImagen/:imageFile',UserController.obtenerImagen);

module.exports = api;
