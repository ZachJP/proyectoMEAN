'use strict'

var express=require('express');
var CancionController=require('../controllers/cancionController');
var api=express.Router();

var md_auth=require('../middleware/midAutenticacion');

var multipart=require('connect-multiparty');//trabajar con la subida de ficheros
var md_upload=multipart({uploadDir: './uploads/audios'});

api.get('/mostrarCancion/:id',md_auth.ensureAuth,CancionController.mostrarCancion);
api.post('/agregarCancion',md_auth.ensureAuth,CancionController.agregarCancion);
api.get('/obtenerCanciones/:album',md_auth.ensureAuth,CancionController.obtenerCanciones);
api.put('/actualizarCancion/:id',md_auth.ensureAuth,CancionController.modificarCancion);
api.delete('/borrarCancion/:id',md_auth.ensureAuth,CancionController.eliminarCancion);
api.post('/subirAudio/:id',[md_upload,md_auth.ensureAuth],CancionController.subirAudio);
api.get('/obtenerAudio/:audioFile',md_auth.ensureAuth,CancionController.obtenerAudio);

module.exports=api;
