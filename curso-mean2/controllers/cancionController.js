'use strict'

var fs=require('fs');
var path=require('path');

var paginacion=require('mongoose-pagination');
var Artista=require('../modelos/artista');
var Album=require('../modelos/album');
var Cancion=require('../modelos/cancion');

function mostrarCancion(req,res){
  var cancionID=req.params.id;

  Cancion.findById(cancionID,(err,cancionStored)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la petición'
      })
    } else {
      if (!cancionStored) {
        return res.status(404).send({
          mensaje: 'No se encontró la canción'
        });
      } else {
        return res.status(200).send({
          mensaje: cancionStored
        });
      }
    }
  });
}
function agregarCancion(req,res){
  var cancion = new Cancion();
  var params=req.body;

  cancion.numPista=params.numPista;
  cancion.nombreTema=params.nombreTema;
  cancion.duracion=params.duracion;
  cancion.pistaAudio=null;
  cancion.imagen=null;
  cancion.album=params.album;

  cancion.save((err,cancionStored)=>{
    if (err) {
      return res.status(500).send({
        mensaje:'Error en el servidor'
      });
    } else {
      if (!cancionStored) {
        return res.status(404).send({
          mensaje:'No se pudo agregar una cancion'
        });
      } else {
        return res.status(200).send({
          mensaje:'Creado correctamente',
          cancion: cancionStored
        });
      }
    }
  });
}
function obtenerCanciones(req,res){
  var albumID=req.params.album;
  if(!albumID){
    var encontrado=Cancion.find({}).sort('numPista');
  }else {
    var encontrado=Cancion.find({album: albumID}).sort('numPista');
  }

  encontrado.populate({
    path:'album',
    populate: {
      path:'artista',
      modelo: 'artista'
    }
  }).exec(function(err,cancionStored){
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la petición'
      });
    } else {
      if (!cancionStored) {
        return res.status(404).send({
          mensaje: 'No hay canciones'
        });
      } else {
        return res.status(200).send({
          mensaje: 'Lista de Canciones',
          canción: cancionStored
        })
      }
    }
  });
}

function modificarCancion(req,res){
  var cancionID=req.params.id;

  var modificar=req.body;

  Cancion.findByIdAndUpdate(cancionID,modificar,(err,cancionStored)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la petición'
      });
    } else {
      if (!cancionStored) {
        return res.status(404).send({
          mensaje: 'No se encontró la canción'
        });
      } else {
        return res.status(200).send({
          cancion : cancionStored
        });
      }
    }
  });
}

function eliminarCancion(req,res){
  var cancionID=req.params.id;

  Cancion.findByIdAndRemove(cancionID,(err,cancionStored)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error de petición'
      });
    } else {
      if (!cancionStored) {
        return res.status(404).send({
          mensaje: 'No se encontró canción'
        });
      } else {
        return res.status(200).send({
          mensaje: 'Eliminado correctamente',
          eliminado: cancionStored
        });
      }
    }
  })
}
function subirAudio(req,res){
  var cancionID=req.params.id;
  var nombre_archivo='No subido... :c';

  if(req.files){
    var archivo_path=req.files.pistaAudio.path;//el path de la pista de audio para la subida del archivo
    var archivo_split=archivo_path.split('\\'); //los separa el nombre de la ubicacion
    var nombre_archivo=archivo_split[2];
    var ext_split=nombre_archivo.split('\.');
    var archivo_ext=ext_split[1]; //los separa la extension del fichero

    if (archivo_ext=='mp3' || archivo_ext=='ogg' || archivo_ext=='wav') {
      Cancion.findByIdAndUpdate(cancionID,{pistaAudio:nombre_archivo},(err,cancionUpdated)=>{
        if (err) {
          return res.status(500).send({mensaje: 'Error en la petición'});
        } else {
          if(!cancionUpdated){
            return res.status(404).send({mensaje: 'No se encontró la canción'});
          }else {
            return res.status(200).send({actualizado:cancionUpdated});
          }
        }
      });
    } else {
      return res.status(200).send({
        message: 'Extensión de audio no válida'
      });
    }
  }else{
    return res.status(200).send({
       message: 'No has subido ningún audio...'
    });
  }
}

function obtenerAudio(req,res){
  var audio=req.params.audioFile;
  var archivo_path='./uploads/audios/'+audio;
  fs.exists(archivo_path,function(exists){
    if (exists) {
      return res.sendFile(path.resolve(archivo_path));
    } else {
      return res.status(200).send({
        message: 'No existe audio'
      });
    }
  });
}
module.exports={
  obtenerAudio,
  subirAudio,
  eliminarCancion,
  modificarCancion,
  obtenerCanciones,
  mostrarCancion,
  agregarCancion
};
