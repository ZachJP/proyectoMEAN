'use strict'

var fs=require('fs');
var path=require('path');
var pagination=require('mongoose-pagination')
var Artista=require('../modelos/artista');
var Album=require('../modelos/album');
var Cancion=require('../modelos/cancion');

function crearArtista(req,res){
  var artista=new Artista();

  var params=req.body;

  artista.nombre=params.nombre;
  artista.descripcion=params.descripcion;
  artista.imagen=null;

  if(artista.nombre!=null && artista.descripcion!=null){
    //Guardar el Usuario
    artista.save((err,artistaStored)=>{
      if(err){
        return res.status(500).send({mensaje: 'Error al registrar el artista'});
      }else {
        if(!artistaStored){
          return res.status(404).send({mensaje: 'No se ha registrado el artista'});
        }else {
          return res.status(200).send({artist: artistaStored});
        }
      }
    });
  }else{
    res.status(200).send({mensaje: 'introduce todo los campos!'});
  }
}

function obtenerArtista(req,res){
  var artistaID=req.params.id;

  Artista.findById(artistaID,(err,artist)=>{
    if(err){
      return res.status(500).send({
        mensaje: 'Error en la petición'
      });
    }else {
      if(!artist){
        return res.status(404).send({
          mensaje: 'El artista no existe'
        });
      }else {
        return res.status(200).send({
          artist
        });
      }
    }
  });
}

function obtenerArtistaTodo(req,res){
  if (req.params.page) {
    var pagina=req.params.page;
  } else {
    var pagina=1;
  }
  var itemsXPagina=3;

  Artista.find().sort('nombre').paginate(pagina, itemsXPagina, function(err,artists,total){
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la petición'
      });
    } else {
      if(!artists){
        return res.status(404).send({
          mensaje: 'No hay Artistas'
        });
      }else {
        return res.status(200).send({
          registros: total,
          artistas: artists,
          paginas: pagina
        });
      }
    }
  });
}

function actualizarArtista(req,res){
  var artistaID=req.params.id;
  var update=req.body;

  Artista.findByIdAndUpdate(artistaID,update,(err,artistUpdated)=>{
    if(err){
      return res.status(500).send({
        mensaje: 'Error al guardar el artista'
      });
    }else {
      if (!artistUpdated) {
        return res.status(404).send({
          mensaje: 'El artista no se ha actualizado'
        })
      } else {
        return res.status(200).send({
          mensaje: 'artista actualizado',
          artistaModificado: artistUpdated
        });
      }
    }
  });
}

function borrarArtista(req,res){
  var artistaID=req.params.id;

  Artista.findByIdAndRemove(artistaID, (err,artistaBorrado)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error al eliminar el artista'
      });
    } else {
      if(!artistaBorrado){
        return res.status(404).send({
          mensaje: 'No encontrado el artista'
        });
      }else{
        return res.status(200).send({
          mensaje: 'Eliminando datos asociados al artista ...'
        });
        Album.find({artistaTemp:artistaBorrado._id}).remove((err,albumBorrado)=>{
          if (err) {
            return res.status(500).send({
              mensaje:'Error al eliminar el album'
            });
          } else {
            if (!albumBorrado) {
              return res.status(404).send({
                mensaje:'No encontrado album'
              });
            } else {
              return res.status(200).send({
                mensaje:'Borrando albumnes...'
              });
              Cancion.find({albumTemp:albumBorrado._id}).remove((err,cancionBorrado)=>{
                if (err) {
                  return res.status(500).send({
                    mensaje:'Error al eliminar la cancion'
                  });
                } else {
                  if (!cancionBorrado) {
                    return res.status(404).send({
                      mensaje:'No encontrado la canción'
                    });
                  } else {
                    return res.status(200).send({
                      mensaje:'Borrando canciones...',
                      artista: artistaBorrado
                    });
                  }
                }
              });
            }
          }
        });
      }
    }
  })
}

function subirImagen(req,res){
  var artistaId=req.params.id;
  var nombre_archivo='No subido... :c';

  if(req.files){
    console.log("Aqui "+path);
    var archivo_path=req.files.imagen.path;//el path del modelo imagen para la subida del archivo
    var archivo_split=archivo_path.split('\\'); //los separa el nombre de la ubicacion
    var nombre_archivo=archivo_split[2];

    var ext_split=nombre_archivo.split('\.');
    var archivo_ext=ext_split[1]; //los separa la extension del fichero
    console.log(ext_split);

    if (archivo_ext=='png' || archivo_ext=='jpg' || archivo_ext=='gif') {
      Artista.findByIdAndUpdate(artistaId,{imagen:nombre_archivo},(err,artistaUpdated)=>{
        if (err) {
          return res.status(500).send({message: 'Error al actualizar el usuario'});
        } else {
          if(!artistaUpdated){
            return res.status(404).send({message: 'No se ha podido actualizar el usuario'});
          }else {
            return res.status(200).send({artista:artistaUpdated});
          }
        }
      });
    } else {
      return res.status(200).send({
        message: 'Extensión de archivo no válida'
      });
    }
  }else{
    return res.status(200).send({
       message: 'No has subido ninguna imagen...'
    });
  }
}

function obtenerImagen(req,res){
  var archivoImagen=req.params.imageFile;
  var archivo_path='./uploads/artistaImagenes/'+archivoImagen;
  fs.exists(archivo_path,function(exists){
    if (exists) {
      return res.sendFile(path.resolve(archivo_path));
    } else {
      return res.status(200).send({
        message: 'No existe archivo'
      });
    }
  });
}
module.exports={
  crearArtista,
  obtenerArtista,
  obtenerArtistaTodo,
  actualizarArtista,
  borrarArtista,
  subirImagen,
  obtenerImagen
};
