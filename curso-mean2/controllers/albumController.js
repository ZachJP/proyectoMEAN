'use strict'

var fs=require('fs');
var path=require('path');

var pagination=require('mongoose-pagination')
var Artista=require('../modelos/artista');
var Album=require('../modelos/album');
var Cancion=require('../modelos/cancion');

function obtenerAlbum(req,res){
  var albumID=req.params.id;

  Album.findById(albumID).populate({path:'artista'}).exec((err, album)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la peticion'
      });
    } else {
      if(!album){
        return res.status(404).send({
          mensaje: 'No se encontro album'
        });
      }else {
        return res.status(200).send({
          album
        })
      }
    }
  });
}
function mostrarAlbums(req,res){
  var artistaID=req.params.artista;
  if (!artistaID) {
    var encontrado=Album.find({}).sort('titulo');
  } else {
    var encontrado=Album.find({artista: artistaID}).sort('año');
  }

  encontrado.populate({path:'artista'}).exec((err,albums)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la petición'
      });
    } else {
        if (!albums) {
          return res.status(404).send({
            mensaje: 'No se ha encontrado Albums'
          });
        } else {
          return res.status(200).send({
            albums
          });
        }
    }
  });
}
function crearAlbum(req,res){
  var album=new Album();
  var params=req.body;
  album.titulo=params.titulo;
  album.descripcion=params.descripcion;
  album.año=params.año;
  album.imagen='null';
  album.artista=params.artista;

  album.save((err,albumCreado)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en el servidor'
      });
    } else {
      if (!albumCreado) {
        return res.status(404).send({
          mensaje:'No se pudo registrar el album'
        });
      } else {
        return res.status(200).send({
          mensaje:'Album registrado',
          album: albumCreado
        });
      }
    }
  });
}
function actualizarAlbum(req,res){
  var albumID=req.params.id;
  var actualizar=req.body;

  Album.findByIdAndUpdate(albumID,actualizar,(err,albumActualizado)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la petición'
      });
    } else {
      if (!albumActualizado) {
        return res.status(404).send({
          mensaje: 'No se pudo realizar la actualizacion'
        });
      } else {
        return res.status(200).send({
          album: albumActualizado
        });
      }
    }
  });
}
function borrarAlbum(req,res){
  var albumID=req.params.id;
  Album.findByIdAndRemove(albumID,(err,borrado)=>{
    if (err) {
      return res.status(500).send({
        mensaje: 'Error en la petición'
      });
    } else {
      if (!borrado) {
        return res.status(404).send({
          mensaje: 'Album no encontrado'
        });
      } else {
        Cancion.find({album: borrado._id}).remove((err,est_cancion)=>{
          if (err) {
            return res.status(500).send({
              mensaje: 'Error en la petición'
            });
          } else {
            if (!est_cancion) {
              return res.status(404).send({
                mensaje: 'canciones no encontradas'
              });
            } else {
              return res.status(200).send({
                mensaje:'canciones asociadas borradas!',
                album: borrado
              });
            }
          }
        });
        // return res.status(200).send({
        //   mensaje: 'Album borrado!',
        //   borrado
        // });
      }
    }
  });
}
function subirImagen(req,res){
  var albumId=req.params.id;
  var nombre_archivo='No subido... :c';
  console.log("Aqui  "+albumId);
  if(req.files){
    console.log(req.files);
    var archivo_path=req.files.imagen.path;//el path del modelo imagen para la subida del archivo
    var archivo_split=archivo_path.split('\\'); //los separa el nombre de la ubicacion
    var nombre_archivo=archivo_split[2];

    console.log('PATH: '+archivo_path);

    var ext_split=nombre_archivo.split('\.');
    var archivo_ext=ext_split[1]; //los separa la extension del fichero

    if (archivo_ext=='png' || archivo_ext=='jpg' || archivo_ext=='gif') {
      Album.findByIdAndUpdate(albumId,{imagen:nombre_archivo},(err,albumUpdated)=>{
        if (err) {
          return res.status(500).send({message: 'Error al subir imagen'});
        } else {
          if(!albumUpdated){
            return res.status(404).send({message: 'No se ha encontrado Imagen'});
          }else {
            return res.status(200).send({album:albumUpdated});
          }
        }
      });
    } else {
      return res.status(200).send({
        message: 'Extensión de archivo no válida'
      });
    }
  }else{
    return res.status(200).send({
       message: 'No has subido ninguna imagen...'
    });
  }
}

function obtenerImagen(req,res){
  var archivoImagen=req.params.imageFile;
  var archivo_path='./uploads/albumImagenes/'+archivoImagen;
  fs.exists(archivo_path,function(exists){
    if (exists) {
      return res.sendFile(path.resolve(archivo_path));
    } else {
      return res.status(200).send({
        message: 'No existe archivo'
      });
    }
  });
}
module.exports={
  obtenerAlbum,
  crearAlbum,
  mostrarAlbums,
  actualizarAlbum,
  borrarAlbum,
  subirImagen,
  obtenerImagen
}
