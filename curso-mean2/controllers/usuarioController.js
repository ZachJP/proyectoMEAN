'use strict'
var bcrypt=require('bcrypt-nodejs'); /*Encriptar Contraseñas*/
var Usuario=require('../modelos/usuario') /*importar modelo*/
var jwt=require('../servicios/jwt') /*importar servicio jwt*/
var fs =require('fs');//importar filesystem
var path=require('path')//acceder a los directorios
function pruebas(req,res){
  res.status(200).send({
      message: 'Probando una acción del controlador Usuarios del API rest'
  });
}

function crearUsuario(req,res){
  var usuario=new Usuario();

  var params=req.body;
  usuario.nombre=params.nombre;
  usuario.nombreCompleto=params.nombreCompleto;
  usuario.correo=params.correo;
  usuario.rol='ROLE_ADMIN';
  usuario.imagen='null';
  if(params.contra){
    //Encriptar Contraseña
    bcrypt.hash(params.contra,null,null,function(err,hash){
      usuario.contra=hash;
      if(usuario.nombre!=null && usuario.nombreCompleto!=null && usuario.correo!=null){
        //Guardar el Usuario
        usuario.save((err,usuarioStored)=>{
          if(err){
            return res.status(500).send({message: 'Error Al guardar el usuario'});
          }else {
            if(!usuarioStored){
              return res.status(404).send({message: 'No se ha registrado el Usuario'});
            }else {
              return res.status(200).send({user: usuarioStored});
            }
          }
        });
      }else{
        return res.status(200).send({message: 'introduce todo los campos!'});
      }
    });
  }else {
    return res.status(500).send({message: 'Introduce la contraseña'});
  }
}

function loginUsuario(req,res){
  var params=req.body;

  var correo=params.correo;
  var contra=params.contra;

  Usuario.findOne({correo:correo.toLowerCase()},(err,user)=>{
    if(err){
      return res.status(500).send({message: 'Error en la petición'});
    }else {
      if (!user) {
        return res.status(404).send({message: 'El usuario no existe'});
      } else {
        //comprobar la contraseña
        bcrypt.compare(contra,user.contra,function(err,check){
          if(check){
            //devolver los datos del usuario
            if(params.getHash){
              //devolver un token de jwt
              return res.status(200).send({
                token:jwt.createToken(user)
              });
            }else {
              return res.status(200).send({user});
            }
          }else {
          }
          return res.status(404).send({message: 'El usuario no ha podido loguearse'});
        })
      }
    }
  });
}

function actualizarUsuario(req,res){
  var userId=req.params.id;
  var update=req.body;

  Usuario.findByIdAndUpdate(userId,update,(err,userUpdated)=>{
    if (err) {
      return res.status(500).send({message: 'Error al actualizar el usuario'});
    } else {
      if(!userUpdated){
        return res.status(404).send({message: 'No se ha podido actualizar el usuario'});
      }else {
        return res.status(200).send({user:userUpdated});
      }
    }
  });
}

function subirImagen(req,res){
  var userId=req.params.id;
  var nombre_archivo='No subido... :c';

  if(req.files){
    var archivo_path=req.files.imagen.path;//el path del modelo imagen para la subida del archivo
    var archivo_split=archivo_path.split('\\'); //los separa el nombre de la ubicacion
    var nombre_archivo=archivo_split[2];

    var ext_split=nombre_archivo.split('\.');
    var archivo_ext=ext_split[1]; //los separa la extension del fichero
    console.log(ext_split);

    if (archivo_ext=='png' || archivo_ext=='jpg' || archivo_ext=='gif') {
      Usuario.findByIdAndUpdate(userId,{imagen:nombre_archivo},(err,userUpdated)=>{
        if (err) {
          return res.status(500).send({message: 'Error al actualizar el usuario'});
        } else {
          if(!userUpdated){
            return res.status(404).send({message: 'No se ha podido actualizar el usuario'});
          }else {
            return res.status(200).send({
              imagen:nombre_archivo,
              user:userUpdated
            });
          }
        }
      });
    } else {
      return res.status(200).send({
        message: 'Extensión de archivo no válida'
      });
    }
  }else{
    return res.status(200).send({
       message: 'No has subido ninguna imagen...'
    });
  }
}

function obtenerImagen(req,res){
  var archivoImagen=req.params.imageFile;
  var archivo_path='./uploads/usuariosImagenes/'+archivoImagen;
  fs.exists(archivo_path,function(exists){
    if (exists) {
      return res.sendFile(path.resolve(archivo_path));
    } else {
      return res.status(200).send({
        message: 'No existe archivo'
      });
    }
  });
}
module.exports = {
  pruebas,
  crearUsuario,
  loginUsuario,
  actualizarUsuario,
  subirImagen,
  obtenerImagen
};
