export class Album{
  constructor(
    public titulo:string,
    public descripcion:string,
    public año:number,
    public imagen: string,
    public artista:string
  ){}
}
