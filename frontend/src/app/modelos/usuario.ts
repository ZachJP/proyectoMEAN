export class Usuario{//Definir una clase
  constructor(
    public _id: string,
    public nombre: string,
    public nombreCompleto: string,
    public correo:string,
    public contra:string,
    public rol: string,
    public imagen: string
  ){}
}
