export class Cancion{
  constructor(
    public numPista:string,
    public nombreTema:string,
    public duracion:string,
    public pistaAudio: string,
    public imagen:string,
    public album: string
  ){}
}
