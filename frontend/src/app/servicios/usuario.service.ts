import { Injectable } from '@angular/core';
import { Http,Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map'; //mapear objetos
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './constantes';

@Injectable()
export class UsuarioService{
  public identidad:string;
  public token:string;
  public url: string;

  constructor(private _http: Http){
    this.url=GLOBAL.url;
  }

  iniciarSesion(usuario, gethash=null){
    if (gethash!=null) {
        usuario.getHash=gethash;
    }
    // return 'Hola mundo desde el servicio';
    let jsonTemp = JSON.stringify(usuario);
    let params=jsonTemp;

    let headers = new Headers({'Content-Type':'application/json'});

    return this._http.post(this.url+'loginUsuario',params,{
      headers: headers
    }).map(res=>res.json());
  }

  obtenerIdentity(){
    let identity=JSON.parse(localStorage.getItem('identity'));
    if (identity!='undefined') {
      this.identidad=identity;
    } else {
      this.identidad=null;
    }
    return this.identidad;
  }
  obtenerToken(){
    let token=JSON.parse(localStorage.getItem('token'));
    if (token!='undefined') {
        this.token=token;
    } else {
      this.token=null;
    }
    return this.token;
  }
}
