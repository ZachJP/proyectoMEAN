import { Component, OnInit } from '@angular/core';
import { Usuario } from './modelos/usuario';
import { UsuarioService} from './servicios/usuario.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UsuarioService]//se cargan todos los servicios
})

export class AppComponent implements OnInit{
  public title = 'Spotify Fake :v';
  public usuario:Usuario;
  public identity;
  public token;
  public msjeError;

  constructor(private _usuarioService: UsuarioService){
    this.usuario=new Usuario('','','','','','ROLE_USER','')
  }

  ngOnInit(){//se genera un codigo apenas se carga el componente
    // var texto=this._usuarioService.iniciarSesion();
    // console.log(texto);
    this.identity=this._usuarioService.obtenerIdentity;
    this.token=this._usuarioService.obtenerToken;

    console.log(this.identity);
    console.log(this.token);
  }
  public entrar(){
    console.log(this.usuario);
    //Conseguir los datos del usuario identificado
    this._usuarioService.iniciarSesion(this.usuario).subscribe(
      response=>{//Todos los datos en el api
        console.log(response);
        let identity=response.user;
        this.identity=identity;
        if(!this.identity._id){
          alert("El usuario no está correctamente logueado");
        }else{
          //Crear elemento en el localStorage para tener el usuario en sesion
          localStorage.setItem('identity',JSON.stringify(identity));
          //Conseguir el token para enviarselo a cada petición http
          this._usuarioService.iniciarSesion(this.usuario,'true').subscribe(
            response=>{//Todos los datos en el api
              let token=response.token;
              this.token=token;
              if(this.token.length<=0){
                alert("El token no ha sido correctamente generado");
              }else{
                localStorage.setItem('token',JSON.stringify(token));

                //Crear elemento en el localStorage para tener el usuario en sesion
                console.log(token);
                console.log(identity);
              }
            },
            error=>{
              var msjeError=<any>error;
              if (msjeError!=null) {
                var body=JSON.parse(error._body);
                  this.msjeError=body.message;
                  console.log(error);
              }
            }
          );
        }
      },
      error=>{
        var msjeError=<any>error;
        if (msjeError!=null) {
          var body=JSON.parse(error._body);
            this.msjeError=body.message;
            console.log(error);
        }
      }
    );
  }

  public salir(){
    localStorage.removeItem('identity');
    localStorage.removeItem('token');
    localStorage.clear();
    this.identity=null;
    this.token=null;

  }
}
